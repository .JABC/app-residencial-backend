﻿using Microsoft.Extensions.DependencyInjection;
using System;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Core.Database;
using WebApi.Core.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;

namespace WebApi
{
    public static class Extensions
    {

        public static IServiceCollection AddStorageSupport(this IServiceCollection services, Action<StorageOptions> options = null)
        {
            var mapping = new MapperConfiguration(cfg =>
                cfg.AddProfile(new EntityProfile()));

            services.AddSingleton(mapping.CreateMapper());

            services.AddDbContext<ResidencialDb>();
            //services.AddSingleton<IStorageProvider, FileStorageProvider>();
            // services.AddTransient<IResidenciaService, ResidenciaJsonService>();
            services.AddTransient<ResidenciaService>();
            services.AddTransient<ResidenteService>();
            services.AddTransient<CalleService>();
            services.AddTransient<VehiculoService>();
            //services.AddTransient<IServiceMethod<VisitanteModel>, VisitanteService>();
            services.AddTransient<VisitaService>();
            //services.AddTransient<IServiceMethod<VisitaGrupoModel>, VisitaGrupoService>();

            services.AddTransient<AuthService>();
            services.AddTransient<UserService>();
            services.AddTransient<RoleService>();
            //services.AddTransient<IServiceMethod<UserInRoleModel>, UserInRoleService>();
            services.AddTransient<EncodeService>();
            //services.AddTransient<IResidenteService, ResidenteJsonService>();
            services.AddTransient<DashboardService>();

            //var opts = new StorageOptions();
            //options?.Invoke(opts);
            //services.AddSingleton(opts);

            return services;
        }

        public static IServiceCollection AddJWTAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(cfg =>
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Authentication:Issuer"],
                    ValidAudience = configuration["Authentication:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(configuration["Llave_super_secreta"])),
                    ClockSkew = TimeSpan.Zero
                });

            return services;
        }

        //public static IApplicationBuilder UseStorageSupport(this IApplicationBuilder app)
        //{

        //    var storageProvider = app.ApplicationServices.GetRequiredService<IStorageProvider>();

        //    storageProvider.LoadData();

        //    return app;
        //}

    }
}
