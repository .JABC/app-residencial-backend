﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using BusinessLayer.Interfaces;
//using BusinessLayer.Models;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Authentication.JwtBearer;

//namespace WebApi.Controllers
//{
//    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
//    [Route("api/[controller]")]
//    [ApiController]
//    public class VisitanteController : ControllerBase
//    {
//        private readonly IServiceMethod<VisitanteModel> _visitanteService;

//        //public VisitanteController(IServiceMethod<VisitanteModel> visitanteService)
//        {
//            _visitanteService = visitanteService;
//        }

//        // [HttpGet("[action]")]
//        // public IActionResult Select()
//        // {
//        //     try
//        //     {
//        //         return Ok(_visitanteService.Select().ToList());
//        //     }
//        //     catch (Exception e)
//        //     {
//        //         return BadRequest(e.Message);
//        //     }
//        // }

//        [HttpPost("[action]")]
//        public IActionResult Select(HttpGetRequest request) {
//            try
//            {
//                return Ok(_visitanteService.GetPage(request));
//            }
//            catch (Exception e) {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Add(VisitanteModel model)
//        {
//            try
//            {
//                _visitanteService.Add(model);
//                return Created("api/[controller]/[action]", model);
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Delete(VisitanteModel model)
//        {
//            try
//            {
//                return Ok(_visitanteService.Delete(model));
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Update(VisitanteModel model)
//        {
//            try
//            {
//                _visitanteService.Update(model);
//                return Ok(model);
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }
//    }
//}

