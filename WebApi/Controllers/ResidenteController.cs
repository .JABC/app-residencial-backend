﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Core.Models;
using WebApi.Core.Services;
using WebApi.Interfaces;
using WebApi.Utilities.Http;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ResidenteController: ControllerBase
    {
        private readonly ResidenteService _service;

        public ResidenteController(ResidenteService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select(OperationRequest operation)
        {
            try
            {
                var result = _service.GetPage(operation);

                OperationResponse<ResidenteModel> response = new OperationResponse<ResidenteModel>()
                {
                    Data = result,
                    TotalCount = result.Capacity
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Add(ResidenteModel model)
        {
            try
            {
                return Ok(_service.Add(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Delete(ResidenteModel model)
        {
            try
            {
                return Ok(_service.Delete(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(ResidenteModel model)
        {
            try
            {
                _service.Update(model);
                return Ok(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult MoverA(ResidenteModel model)
        {
            try
            {
                return Ok(_service.MoverA(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
