﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.Core.Models;
using WebApi.Utilities.Http;
using WebApi.Core.Services;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ResidenciaController: ControllerBase
    {
        private readonly ResidenciaService _service;

        public ResidenciaController(ResidenciaService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select(OperationRequest operation)
        {
            try
            {
                var result = _service.GetPage(operation);

                OperationResponse<ResidenciaModel> response = new OperationResponse<ResidenciaModel>()
                {
                    Data = result,
                    TotalCount = result.Capacity
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Add(ResidenciaModel model)
        {
            try
            {
                return Ok(_service.Add(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Delete(ResidenciaModel model)
        {
            try
            {
                return Ok(_service.Delete(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(ResidenciaModel model)
        {
            try
            {
                _service.Update(model);
                return Ok(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


    }
}
