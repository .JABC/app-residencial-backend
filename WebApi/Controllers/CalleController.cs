﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Utilities.Http;
using WebApi.Interfaces;
using WebApi.Core.Models;
using Microsoft.AspNetCore.Authorization;
using WebApi.Core.Services;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class CalleController: ControllerBase
    {
        private readonly CalleService _service;

        public CalleController(CalleService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select(OperationRequest operation)
        {
            try
            {
                var result = _service.GetPage(operation);

                OperationResponse<CalleModel> response = new OperationResponse<CalleModel>()
                {
                    Data = result,
                    TotalCount = result.Capacity
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Add(CalleModel model)
        {
            try
            {
                return Ok(_service.Add(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Delete(CalleModel model)
        {
            try
            {
                return Ok(_service.Delete(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(CalleModel model)
        {
            try
            {
                _service.Update(model);
                return Ok(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
