﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Core.Models;
using WebApi.Core.Services;
using WebApi.Interfaces;
using WebApi.Utilities.Http;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculoController : ControllerBase
    {
        private readonly VehiculoService _service;

        public VehiculoController(VehiculoService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select(OperationRequest operation)
        {
            try
            {
                var result = _service.GetPage(operation);

                OperationResponse<VehiculoModel> response = new OperationResponse<VehiculoModel>()
                {
                    Data = result,
                    TotalCount = result.Capacity
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Add(VehiculoModel model)
        {
            try
            {
                return Ok(_service.Add(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Delete(VehiculoModel model)
        {
            try
            {
                return Ok(_service.Delete(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(VehiculoModel model)
        {
            try
            {
                _service.Update(model);
                return Ok(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
