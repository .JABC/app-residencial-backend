﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.Core.Models;
using WebApi.Utilities.Http;
using WebApi.Core.Services;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class VisitaController : ControllerBase
    {
        private readonly VisitaService _service;

        public VisitaController(VisitaService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select(OperationRequest operation)
        {
            try
            {
                var result = _service.GetPage(operation);

                OperationResponse<VisitaModel> response = new OperationResponse<VisitaModel>()
                {
                    Data = result,
                    TotalCount = result.Capacity
                };

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Add(VisitaModel model)
        {
            try
            {
                return Ok(_service.Add(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Delete(VisitaModel model)
        {
            try
            {
                return Ok(_service.Delete(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(VisitaModel model)
        {
            try
            {
                _service.Update(model);
                return Ok(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Reportar(VisitaModel model)
        {
            try
            {
                return Ok(_service.Reportar(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult DarEntrada(VisitaModel model)
        {
            try
            {
                return Ok(_service.DarEntrada(model));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}

