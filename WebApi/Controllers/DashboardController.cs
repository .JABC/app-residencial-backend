﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebApi.Core.Models;
using WebApi.Core.Services;
using WebApi.Interfaces;
using WebApi.Utilities.Http;

namespace WebApi.Controllers
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly DashboardService _service;

        public DashboardController(DashboardService service)
        {
            _service = service;
        }

        [HttpPost("[action]")]
        public IActionResult Select()
        {
            try
            {
 		var data = _service.GetData();
		return Ok(new { Data = new[] { data }, TotalCount = 0 });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}

