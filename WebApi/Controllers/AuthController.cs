﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApi.Core.Models;
using WebApi.Core.Services;
using WebApi.Interfaces;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;
        private readonly IConfiguration _configuration;

        public AuthController(
            AuthService authService,
            IConfiguration configuration)
        {
            _authService = authService;
            _configuration = configuration;
        }

        [HttpPost("[action]")]
        public IActionResult LogIn(UserModel model)
        {
            try
            {
                var user = _authService.ValidateUser(model);
                return Ok(BuildToken(user));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Create(UserModel model)
        {
            try
            {
                return Ok(
                    _authService.CreateUser(
                        model.UserName,
                        model.Email,
                        model.Password,
                        model.RoleId));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("[action]")]
        public IActionResult ChangePassword(UserPasswordModel model)
        {
            try
            {
                return Ok(_authService.ChangePassword(model));
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

        [NonAction]
        public IActionResult BuildToken(UserModel model)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, model.UserName),
                new Claim(JwtRegisteredClaimNames.Email, model.Email),
                new Claim("role", model.RoleId.Value.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Llave_super_secreta"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["Authentication:Issuer"],
                audience: _configuration["Authentication:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(double.Parse(_configuration["Authentication:Expires"])),
                signingCredentials: creds);

            return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
        }
    }
}
