﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using BusinessLayer.Interfaces;
//using BusinessLayer.Models;
//using Microsoft.AspNetCore.Authentication.JwtBearer;
//using Microsoft.AspNetCore.Authorization;

//namespace WebApi.Controllers
//{
//    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
//    [Route("api/[controller]")]
//    [ApiController]
//    public class VisitaGrupoController : ControllerBase
//    {
//        private readonly IServiceMethod<VisitaGrupoModel> _visitaGrupoService;

//        public VisitaGrupoController(IServiceMethod<VisitaGrupoModel> visitaGrupoService)
//        {
//            _visitaGrupoService = visitaGrupoService;
//        }

//        // [HttpGet("[action]")]
//        // public IActionResult Select()
//        // {
//        //     try
//        //     {
//        //         return Ok(_visitaGrupoService.Select().ToList());
//        //     }
//        //     catch (Exception e)
//        //     {
//        //         return BadRequest(e.Message);
//        //     }
//        // }

//        [HttpPost("[action]")]
//        public IActionResult Select(HttpGetRequest request) {
//            try
//            {
//                return Ok(_visitaGrupoService.GetPage(request));
//            }
//            catch (Exception e) {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Add(VisitaGrupoModel model)
//        {
//            try
//            {
//                _visitaGrupoService.Add(model);
//                return Created("api/[controller]/[action]", model);
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Delete(VisitaGrupoModel model)
//        {
//            try
//            {
//                return Ok(_visitaGrupoService.Delete(model));
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }

//        [HttpPost("[action]")]
//        public IActionResult Update(VisitaGrupoModel model)
//        {
//            try
//            {
//                _visitaGrupoService.Update(model);
//                return Ok(model);
//            }
//            catch (Exception e)
//            {
//                return BadRequest(e.Message);
//            }
//        }
//    }
//}

