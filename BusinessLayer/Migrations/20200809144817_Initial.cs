﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Core.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Calle",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 20, nullable: true),
                    Distancia = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Calle", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Parentesco",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parentesco", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoResidencia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoResidencia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoResidente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoResidente", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    EncodeString = table.Column<string>(nullable: true),
                    IsLocked = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    RoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Residencia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Numero = table.Column<string>(nullable: true),
                    Piso = table.Column<int>(nullable: true),
                    Poblada = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    UniqueIdentifier = table.Column<string>(nullable: true),
                    PropietarioId = table.Column<int>(nullable: true),
                    CalleId = table.Column<int>(nullable: true),
                    TipoResidenciaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residencia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residencia_Calle_CalleId",
                        column: x => x.CalleId,
                        principalTable: "Calle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Residencia_TipoResidencia_TipoResidenciaId",
                        column: x => x.TipoResidenciaId,
                        principalTable: "TipoResidencia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Residente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DNI = table.Column<string>(nullable: true),
                    Nombre = table.Column<string>(maxLength: 20, nullable: true),
                    Apellido = table.Column<string>(maxLength: 20, nullable: true),
                    Telefono = table.Column<string>(nullable: true),
                    Nacimiento = table.Column<DateTime>(type: "date", nullable: true),
                    Genero = table.Column<string>(maxLength: 1, nullable: true),
                    Activo = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: true),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    TipoResidenteId = table.Column<int>(nullable: true),
                    ParentescoId = table.Column<int>(nullable: true),
                    ResidenciaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Residente", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Residente_Parentesco_ParentescoId",
                        column: x => x.ParentescoId,
                        principalTable: "Parentesco",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Residente_Residencia_ResidenciaId",
                        column: x => x.ResidenciaId,
                        principalTable: "Residencia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Residente_TipoResidente_TipoResidenteId",
                        column: x => x.TipoResidenteId,
                        principalTable: "TipoResidente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Visita",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VisitanteDNI = table.Column<string>(nullable: true),
                    VisitanteNombre = table.Column<string>(nullable: true),
                    Motivo = table.Column<string>(maxLength: 15, nullable: true),
                    Llegada = table.Column<DateTime>(nullable: true),
                    Salida = table.Column<DateTime>(nullable: true),
                    Nota = table.Column<string>(nullable: true),
                    Reportado = table.Column<bool>(nullable: false),
                    DarEntrada = table.Column<DateTime>(nullable: true),
                    ResidenciaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visita", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visita_Residencia_ResidenciaId",
                        column: x => x.ResidenciaId,
                        principalTable: "Residencia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Vehiculo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Matricula = table.Column<string>(maxLength: 20, nullable: true),
                    Descripcion = table.Column<string>(maxLength: 30, nullable: true),
                    ResidenteId = table.Column<int>(nullable: true),
                    ResidenciaEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehiculo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehiculo_Residencia_ResidenciaEntityId",
                        column: x => x.ResidenciaEntityId,
                        principalTable: "Residencia",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Vehiculo_Residente_ResidenteId",
                        column: x => x.ResidenteId,
                        principalTable: "Residente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.InsertData(
                table: "Parentesco",
                columns: new[] { "Id", "Descripcion" },
                values: new object[,]
                {
                    { 1, "Ninguno" },
                    { 2, "Hermano" },
                    { 3, "Esposo" },
                    { 4, "Primo" },
                    { 5, "Sobrino" },
                    { 6, "Padre" },
                    { 7, "Hijo" },
                    { 8, "Otro" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 3, "Residente" },
                    { 1, "Administrador" },
                    { 2, "Guardia" }
                });

            migrationBuilder.InsertData(
                table: "TipoResidencia",
                columns: new[] { "Id", "Descripcion" },
                values: new object[,]
                {
                    { 1, "Apartamento" },
                    { 2, "Casa" },
                    { 3, "Negocio" }
                });

            migrationBuilder.InsertData(
                table: "TipoResidente",
                columns: new[] { "Id", "Descripcion" },
                values: new object[,]
                {
                    { 2, "Inquilino" },
                    { 1, "Propietario" },
                    { 3, "Habitante" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Residencia_CalleId",
                table: "Residencia",
                column: "CalleId");

            migrationBuilder.CreateIndex(
                name: "IX_Residencia_PropietarioId",
                table: "Residencia",
                column: "PropietarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Residencia_TipoResidenciaId",
                table: "Residencia",
                column: "TipoResidenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Residencia_Numero_CalleId",
                table: "Residencia",
                columns: new[] { "Numero", "CalleId" },
                unique: true,
                filter: "[Numero] IS NOT NULL AND [CalleId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Residente_DNI",
                table: "Residente",
                column: "DNI",
                unique: true,
                filter: "[DNI] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Residente_ParentescoId",
                table: "Residente",
                column: "ParentescoId");

            migrationBuilder.CreateIndex(
                name: "IX_Residente_ResidenciaId",
                table: "Residente",
                column: "ResidenciaId");

            migrationBuilder.CreateIndex(
                name: "IX_Residente_TipoResidenteId",
                table: "Residente",
                column: "TipoResidenteId");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleId",
                table: "User",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehiculo_ResidenciaEntityId",
                table: "Vehiculo",
                column: "ResidenciaEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehiculo_ResidenteId",
                table: "Vehiculo",
                column: "ResidenteId");

            migrationBuilder.CreateIndex(
                name: "IX_Visita_ResidenciaId",
                table: "Visita",
                column: "ResidenciaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Residencia_Residente_PropietarioId",
                table: "Residencia",
                column: "PropietarioId",
                principalTable: "Residente",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residencia_Calle_CalleId",
                table: "Residencia");

            migrationBuilder.DropForeignKey(
                name: "FK_Residencia_Residente_PropietarioId",
                table: "Residencia");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Vehiculo");

            migrationBuilder.DropTable(
                name: "Visita");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Calle");

            migrationBuilder.DropTable(
                name: "Residente");

            migrationBuilder.DropTable(
                name: "Parentesco");

            migrationBuilder.DropTable(
                name: "Residencia");

            migrationBuilder.DropTable(
                name: "TipoResidente");

            migrationBuilder.DropTable(
                name: "TipoResidencia");
        }
    }
}
