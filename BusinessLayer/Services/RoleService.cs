﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class RoleService: IServiceMethod<RoleModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public RoleService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public RoleModel Add(RoleModel model)
        {

            var entity = _mapper.Map<RoleModel, RoleEntity>(model);
            _residencialDb.Roles.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(RoleModel model)
        {
            var item = _residencialDb.Roles.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Roles.Remove(item);

            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<RoleModel> Select()
        {
            var query = _residencialDb.Roles.ProjectTo<RoleModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public RoleModel FindBy(Func<RoleModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<RoleModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public RoleModel Update(RoleModel model)
        {
            var item = _residencialDb.Roles.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }
    }
}
