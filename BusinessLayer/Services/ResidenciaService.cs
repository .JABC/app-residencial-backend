﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class ResidenciaService : IServiceMethod<ResidenciaModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public ResidenciaService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public ResidenciaModel Add(ResidenciaModel model)
        {
            var entity = _mapper.Map<ResidenciaModel, ResidenciaEntity>(model);

            if (_residencialDb.Residencias.Any(x => x.Numero == entity.Numero))
                throw new Exception("El numero de Residencia ya existe");

            entity.CreateDate = DateTime.Now;
            entity.UniqueIdentifier = Guid.NewGuid().ToString("x").GetHashCode().ToString("x");

            _residencialDb.Residencias.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(ResidenciaModel model)
        {
            var item = _residencialDb.Residencias.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Residencias.Remove(item);
            return _residencialDb.SaveChanges() > 0;

        }

        public IQueryable<ResidenciaModel> Select()
        {
            var query = _residencialDb.Residencias.ProjectTo<ResidenciaModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public ResidenciaModel FindBy(Func<ResidenciaModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<ResidenciaModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public ResidenciaModel Update(ResidenciaModel model)
        {
            var item = _residencialDb.Residencias.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            model.UpdateDate = DateTime.Now;

            _mapper.Map(model, item);

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }
    }
}
