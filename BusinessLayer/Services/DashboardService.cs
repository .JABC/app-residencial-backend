using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class DashboardService
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public DashboardService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public IQueryable<TModel> Select<TModel, TEntity>() where TEntity: class
        {
            var query = _residencialDb.Set<TEntity>().ProjectTo<TModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public Dictionary<string, object> GetResidenteData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<ResidenteModel, ResidenteEntity>();
            var total = data.Count();
            var activos = data.Where(x => x.Activo == true).Count();
            var propietario = data.Where(x => x.TipoResidenteId == (int)ETipoResidente.Propietario).Count();
            var inquilino = data.Where(x => x.TipoResidenteId == (int)ETipoResidente.Inquilino).Count();
            var habitante = data.Where(x => x.TipoResidenteId == (int)ETipoResidente.Habitante).Count();
            var otros = data.Where(x => x.TipoResidenteId == null).Count();
            var masculino = data.Where(x => x.Genero == "M").Count();
            var femenino = data.Where(x => x.Genero == "F").Count();
            var lastMonth = data.Where(x => x.CreateDate.Value.Month == DateTime.Now.AddMonths(-1).Month).Count();
            var beforeLastMonth = data.Where(x => x.CreateDate.Value.Month == DateTime.Now.AddMonths(-2).Month).Count();
            var residentePorCasa = data.Where(x => x.ResidenciaId != null).Count();

            chartData["activos"] = activos;
            chartData["propietario"] = propietario;
            chartData["inquilino"] = inquilino;
            chartData["habitante"] = habitante;
            chartData["otros"] = otros;
            chartData["masculino"] = masculino;
            chartData["femenino"] = femenino;
            chartData["lastMonth"] = lastMonth;
            chartData["beforeLastMonth"] = beforeLastMonth;
            chartData["residentePorCasa"] = residentePorCasa;
            chartData["total"] = total;

            return chartData;
        }

        public Dictionary<string, object> GetResidenciaData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<ResidenciaModel, ResidenciaEntity>();
            var total = data.Count();
            var poblada = data.Where(x => x.Poblada == true).Count();
            var casa = data.Where(x => x.TipoResidenciaId == (int)ETipoResidencia.Casa).Count();
            var apartamento = data.Where(x => x.TipoResidenciaId == (int)ETipoResidencia.Apartamento).Count();
            var negocio = data.Where(x => x.TipoResidenciaId == (int)ETipoResidencia.Negocio).Count();
            var otros = data.Where(x => x.TipoResidenciaId == null).Count();
            var lastMonth = data.Where(x => x.CreateDate.Value.Month == DateTime.Now.AddMonths(-1).Month).Count();
            var beforeLastMonth = data.Where(x => x.CreateDate.Value.Month == DateTime.Now.AddMonths(-2).Month).Count();

            chartData["poblada"] = poblada;
            chartData["casa"] = casa;
            chartData["apartamento"] = apartamento;
            chartData["negocio"] = negocio;
            chartData["otros"] = otros;
            chartData["lastMonth"] = lastMonth;
            chartData["beforeLastMonth"] = beforeLastMonth;
            chartData["total"] = total;

            return chartData;
        }

        public Dictionary<string, object> GetVehiculoData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<VehiculoModel, VehiculoEntity>();
            var total = data.Count();
            var vehiculoPorCasa = data.Where(x => x.ResidenteId != null).Count();

            chartData["total"] = total;
            chartData["vehiculoPorCasa"] = vehiculoPorCasa;
            return chartData;
        }

        public Dictionary<string, object> GetCalleData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<CalleModel, CalleEntity>();
            var total = data.Count();
            chartData["total"] = total;
            return chartData;
        }

        public Dictionary<string, object> GetUsuarioData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<UserModel, UserEntity>();
            var total = data.Count();
            chartData["total"] = total;
            return chartData;
        }

        public Dictionary<string, object> GetVisitaData()
        {
            var chartData = new Dictionary<string, object>();
            var data = Select<VisitaModel, VisitaEntity>();
            var total = data.Count();
            var thisMonth = data.Where(x => DateTime.Parse(x.Llegada).Month == DateTime.Now.Month);
            var lastMonth = data.Where(x => DateTime.Parse(x.Llegada).Month == DateTime.Now.AddMonths(-1).Month);
            var beforeLastMonth = data.Where(x => DateTime.Parse(x.Llegada).Month == DateTime.Now.AddMonths(-2).Month);
            var series = new int[12];

            for (int i = 0; i < 12; i++)
            {
                var index = i;
                var dateNow = DateTime.Now.AddMonths(-index);
                var visitaByMonth = data.Where(x => DateTime.Parse(x.Llegada).Month == dateNow.Month
                    && DateTime.Parse(x.Llegada).Year == dateNow.Year).Count();
               series[dateNow.Month - 1] = visitaByMonth;
            }

            chartData["thisMonth"] = new {
                data = thisMonth.ToList().TakeLast(6),
                count = thisMonth.Count()
            };
            chartData["lastMonth"] = new {
                data = lastMonth.ToList().TakeLast(6),
                count = lastMonth.Count()
            };
            chartData["beforeLastMonth"] = new {
                data = beforeLastMonth.ToList().TakeLast(6),
                count = beforeLastMonth.Count()
            };
            chartData["series"] = series;
            chartData["total"] = total;

            return chartData;
        }

        public Dictionary<string, object> GetData()
        {
            var data = new Dictionary<string, object>();
            data["residente"] = GetResidenteData();
            data["residencia"] = GetResidenciaData();
            data["calle"] = GetCalleData();
            data["vehiculo"] = GetVehiculoData();
            data["usuario"] = GetUsuarioData();
	    data["visita"] = GetVisitaData();

            return data;
        }

    }
}

