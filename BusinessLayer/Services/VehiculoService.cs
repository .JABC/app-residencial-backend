﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class VehiculoService: IServiceMethod<VehiculoModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public VehiculoService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public VehiculoModel Add(VehiculoModel model)
        {
            var entity = _mapper.Map<VehiculoModel, VehiculoEntity>(model);

            if (_residencialDb.Vehiculos.Any(x => x.Matricula == entity.Matricula))
                throw new Exception("La matricula del vehiculo ya existe");

            _residencialDb.Vehiculos.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(VehiculoModel model)
        {
            var item = _residencialDb.Vehiculos.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Vehiculos.Remove(item);

            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<VehiculoModel> Select()
        {
            var query = _residencialDb.Vehiculos.ProjectTo<VehiculoModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public VehiculoModel FindBy(Func<VehiculoModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<VehiculoModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public VehiculoModel Update(VehiculoModel model)
        {
            var item = _residencialDb.Vehiculos.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }
    }
}
