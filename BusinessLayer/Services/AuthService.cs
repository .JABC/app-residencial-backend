﻿using System;
using System.Linq;
using WebApi.Core.Models;
using WebApi.Interfaces;

namespace WebApi.Core.Services
{
    public class AuthService : IAuthService
    {
        private readonly UserService _userService;
        //private readonly IServiceMethod<RoleModel> _roleService;
        //private readonly IServiceMethod<UserInRoleModel> _userInRoleService;
        private readonly EncodeService _encodeService;

        public AuthService(
            UserService userService,
            /*IServiceMethod<RoleModel> roleService,*/
            /*IServiceMethod<UserInRoleModel> userInRoleService,*/
            EncodeService encodeService)
        {
            _userService = userService;
            //_roleService = roleService;
            //_userInRoleService = userInRoleService;
            _encodeService = encodeService;
        }

        public UserModel ValidateUser(UserModel model)
        {
            var userByName = _userService.FindBy(x => x.UserName == model.Email);
            var userByEmail = _userService.FindBy(x => x.Email == model.Email);

            if (userByEmail != null && IsUserValid(userByEmail, model.Password))
            {
                return userByEmail;
            } else if (userByName != null && IsUserValid(userByName, model.Password))
            {
                return userByName;
            }

            throw new Exception("Nombre de usuario o correo incorrectos. Vuelva a intentarlo");
        }

        //public UserModel CreateUser(string username, string email, string password)
        //{
        //    return CreateUser(username, email, password, roles: null);
        //}

        //public UserModel CreateUser(string username, string email, string password, string roleName)
        //{
        //    return CreateUser(username, email, password, roles: new[] { roleName });
        //}

        public UserModel CreateUser(string username, string email, string password, int? roleId)
        {
            if (!_userService.Select().Any())
            { // Sin no existen usuarios el primero de ellos sera Administrador
                roleId = (int)ERoles.Administrador; 
            }

            if (roleId == null) roleId = (int)ERoles.Residente;

            if (_userService.Select().Any(x => x.UserName.ToLower() == username.ToLower()))
            {
                throw new Exception("El nombre de usuario ya existe");
            } else if (_userService.Select().Any(x => x.Email.ToLower() == email.ToLower()))
            {
                throw new Exception("El correo electronico ya existe");
            }

            var encodeString = _encodeService.GenerateEncodeBytes();

            UserModel user = new UserModel
            {
                RoleId = roleId,
                UserName = username,
                Email = email,
                Password = _encodeService.EncodePassword(encodeString, password),
                EncodeString = encodeString,
                IsLocked = false,
                CreateDate = DateTime.Now
            };

            _userService.Add(user);

            return user;
        }

        public bool ChangePassword(UserPasswordModel model)
        {
            var user = _userService.FindBy(x => x.UserName == model.Username);
            var encodePassword = _encodeService.EncodePassword(user.EncodeString, model.OldPassword);

            if (user == null) return false;

            if (user.Password != encodePassword) throw new Exception($"La contraseña actual no es correcta");

            user.EncodeString = _encodeService.GenerateEncodeBytes();
            user.Password = _encodeService.EncodePassword(user.EncodeString, model.NewPassword);
            _userService.Update(user);
            return true;
        }

        //private IQueryable<RoleModel> GetUserRoles(int identity)
        //{
        //    var userInRoles = _userInRoleService
        //        .FindBy(x => x.UserKey == userKey).ToList();
        //    if (userInRoles != null && userInRoles.Count > 0)
        //    {
        //        var userRoleKeys = userInRoles.Select(
        //        x => x.RoleKey).ToArray();

        //    var userRoles = _roleService
        //        .FindBy(x => userRoleKeys.Contains(x.Key));
        //        return userRoles;
        //    }
        //    return Enumerable.Empty<RoleModel>();
        //}

        //public int AddRole(string roleName)
        //{
        //    var role = _roleService.FindBy(x => x.Name == roleName);

        //    if (role == null)
        //    {
        //        role = _roleService.FindBy(y => y.Name == "Residente");
        //    }

        //    return role.Id;
        //}

        //public bool RemoveUserRole(int userId, int roleId)
        //{
        //    var item = from n in _userInRoleService.Select()
        //               where n.UserId == userId
        //               && n.RoleId == roleId
        //               select n;

        //    if (item.FirstOrDefault() == null)
        //    {
        //        throw new Exception("Registro no encontrado");
        //    }

        //    return _userInRoleService.Delete(item.FirstOrDefault());

        //}

        public bool IsUserValid(UserModel user, string password)
        {
            if (!IsPasswordValid(user, password))
            {
                throw new Exception("Su contraseña es incorrecta. Vuelva a intentarlo");
            }

            return !user.IsLocked;
        }

        public bool IsPasswordValid(UserModel user, string password)
        {
            return string.Equals(
                _encodeService.EncodePassword(user.EncodeString, password),
                user.Password);
        }

    }

}
