﻿//using AutoMapper;
//using AutoMapper.QueryableExtensions;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using WebApi.Core.Database;
//using WebApi.Core.Database.Entities;
//using WebApi.Core.Models;
//using WebApi.Interfaces;

//namespace WebApi.Core.Services
//{
//    public class VisitanteService: IServiceMethod<VisitanteModel>
//    {
//        private readonly ResidencialDb _residencialDb;
//        private readonly IMapper _mapper;

//        public VisitanteService(ResidencialDb residencialDb, IMapper mapper)
//        {
//            _residencialDb = residencialDb;
//            _mapper = mapper;
//        }

//        public int Add(VisitanteModel model)
//        {

//            var entity = _mapper.Map<VisitanteModel, VisitanteEntity>(model);

//            if (_residencialDb.Visitantes.Any(x => x.DNI == entity.DNI))
//                throw new Exception("El DNI del Visitante ya existe.");

//            _residencialDb.Visitantes.Add(entity);
//            _residencialDb.SaveChanges();

//            return entity.Id;
//        }

//        public bool Delete(VisitanteModel model)
//        {
//            var item = _residencialDb.Visitantes.FirstOrDefault(x => x.Id == model.Id);

//            if (item == null)
//                throw new Exception("Registro No Existe");

//            _residencialDb.Visitantes.Remove(item);

//            return _residencialDb.SaveChanges() > 0;
//        }

//        public IEnumerable<VisitanteModel> Select()
//        {
//            var query = _residencialDb.Visitantes.ProjectTo<VisitanteModel>(_mapper.ConfigurationProvider);
//            return query;
//        }

//        public VisitanteModel FindBy(Func<VisitanteModel, bool> predicate)
//        {
//            return Select().Where(predicate).FirstOrDefault();
//        }

//        public HttpGetResult<VisitanteModel> GetPage(HttpGetRequest request)
//        {
//            var result = new HttpGetResult<VisitanteModel>();
//            var paginate = new Paginate();

//            if (string.IsNullOrEmpty(request.FilterValue))
//            {

//                result.Data = Select();

//            }
//            else if (request.FilterValue.Contains(','))
//            { // Reparar solo acepta dos valores

//                var splitString = request.FilterValue.Split(',');
//                var items = new List<VisitanteModel>();
//                foreach (var model in Select())
//                {
//                    try
//                    {
//                        var item = JsonConvert.SerializeObject(model).ToLower();
//                        if (item.Contains(splitString[0].Trim().ToLower())
//                            && item.Contains(splitString[1].Trim().ToLower()))
//                        {
//                            items.Add(model);
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        throw new Exception(e.Message);
//                    }
//                }

//                result.Data = items;

//            }
//            else
//            {

//                var items = new List<VisitanteModel>();
//                foreach (var model in Select())
//                {
//                    var item = JsonConvert.SerializeObject(model).ToLower();
//                    if (item.Contains(request.FilterValue.ToLower()))
//                    {
//                        items.Add(model);
//                    }
//                }

//                result.Data = items;
//            }


//            paginate.CurrentPage = request.Page.CurrentPage;
//            paginate.Limit = request.Page.Limit;
//            paginate.Count = Select().Count();

//            result.Data = result.Data
//                            .Skip((paginate.CurrentPage - 1) * paginate.Limit)
//                            .Take(paginate.Limit).AsEnumerable();

//            result.Page = paginate;

//            return result;
//        }

//        public void Update(VisitanteModel model)
//        {
//            var item = _residencialDb.Visitantes.FirstOrDefault(x => x.Id == model.Id);

//            if (item == null)
//                throw new Exception("Registro No Encontrado");

//            _mapper.Map(model, item);

//            _residencialDb.SaveChanges();
//        }
//    }
//}
