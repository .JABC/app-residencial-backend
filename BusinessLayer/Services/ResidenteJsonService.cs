﻿//using BusinessLayer.Interfaces;
//using BusinessLayer.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace BusinessLayer.Services
//{
//    public class ResidenteJsonService : IResidenteService
//    {
//        private readonly IStorageProvider _provider;

//        public ResidenteJsonService(IStorageProvider provider)
//        {
//            _provider = provider;
//        }

//        public int Add( ResidenteModel model)
//        {
//            model.Id = _provider.Residentes.Count + 1;
//            model.FechaNacimiento.ToShortDateString();
//            _provider.Residentes.Add(model);
//            _provider.SaveData();

//            return model.Id;
 
//        }

//        public bool Delete(int id)
//        {
//            var item = _provider.Residentes.Find(x => x.Id == id);

//            if (item == null)
//                throw new Exception("No existe...");
//            _provider.Residentes.Remove(item);
//            _provider.SaveData();

//            return true;

//        }

//        public IEnumerable<ResidenteModel> Select()
//        {
//            throw new NotImplementedException();
//        }

//        public ResidenteModel SelectOne(int id)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable<ResidenteModel> Show()
//        {
//            return _provider.Residentes;
//        }

//        public void Update(ResidenteModel model)
//        {
//            var item = _provider.Residentes.Find(x => x.Id == model.Id);

//            if (item == null)
//                throw new Exception("Elemento no encontrado...");

//            item.Nombre = model.Nombre;
//            item.Apellido = model.Apellido;
//            item.CodigoIdent = model.CodigoIdent;
//            item.FechaNacimiento = model.FechaNacimiento;
//            item.Genero = model.Genero;
//            item.TipoResidente = model.TipoResidente;
//            item.Activo = model.Activo;

//            _provider.SaveData();
//        }
//    }
//}
