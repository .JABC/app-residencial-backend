﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class UserService: IServiceMethod<UserModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public UserService(
            ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public UserModel Add(UserModel model)
        {

            var entity = _mapper.Map<UserModel, UserEntity>(model);

            _residencialDb.Users.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(UserModel model)
        {
            var item = _residencialDb.Users.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Users.Remove(item);

            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<UserModel> Select()
        {
            var query = _residencialDb.Users.ProjectTo<UserModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public UserModel FindBy(Func<UserModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<UserModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public UserModel Update(UserModel model)
        {
            var item = _residencialDb.Users.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);
            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }
    }
}
