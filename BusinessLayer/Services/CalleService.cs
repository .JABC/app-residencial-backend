﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class CalleService : IServiceMethod<CalleModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public CalleService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public CalleModel Add(CalleModel model)
        {

            var entity = _mapper.Map<CalleModel, CalleEntity>(model);
            _residencialDb.Calles.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(CalleModel model)
        {
            var item = _residencialDb.Calles.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            if (model.Residencias.Count > 0)
                throw new Exception("No se puede eliminar porque tiene residencias asociadas");

            _residencialDb.Calles.Remove(item);

            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<CalleModel> Select()
        {
            var query = _residencialDb.Calles.ProjectTo<CalleModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public CalleModel FindBy(Func<CalleModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<CalleModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public CalleModel Update(CalleModel model)
        {
            var item = _residencialDb.Calles.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }
    }
}
