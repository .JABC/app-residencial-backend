﻿using System;
using System.Security.Cryptography;
using System.Text;
using WebApi.Interfaces;

namespace WebApi.Core.Services
{
    public class EncodeService : IEncodeService
    {
        public EncodeService() { }

        public string GenerateEncodeBytes()
        {
            var data = new byte[0x10];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetBytes(data);
                return Convert.ToBase64String(data);
            }

        }

        public string EncodePassword(string encode, string password)
        {
            using (var sha256 = SHA256.Create())
            {
                var encodePassword = string.Format("{0}{1}", encode, password);
                byte[] encodePasswordAsBytes = Encoding.UTF8.GetBytes(encodePassword);

                return Convert.ToBase64String(
                    sha256.ComputeHash(encodePasswordAsBytes));
            }
        }

    }
}
