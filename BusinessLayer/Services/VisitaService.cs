﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class VisitaService: IServiceMethod<VisitaModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public VisitaService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public VisitaModel Add(VisitaModel model)
        {

            var entity = _mapper.Map<VisitaModel, VisitaEntity>(model);
            _residencialDb.Visitas.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(VisitaModel model)
        {
            var item = _residencialDb.Visitas.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Visitas.Remove(item);

            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<VisitaModel> Select()
        {
            var query = _residencialDb.Visitas.ProjectTo<VisitaModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public VisitaModel FindBy(Func<VisitaModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<VisitaModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public VisitaModel Update(VisitaModel model)
        {
            var item = _residencialDb.Visitas.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }

        public bool Reportar(VisitaModel model)
        {
            var visitas = _residencialDb.Visitas.Where(x => x.VisitanteDNI == model.VisitanteDNI);

            foreach (VisitaEntity visita in visitas)
            {
                visita.Reportado = model.Reportado;
            }

            _residencialDb.SaveChanges();

            return FindBy(x => x.VisitanteDNI == model.VisitanteDNI) != null;
        }

        public bool DarEntrada(VisitaModel model)
        {
            var visitas = _residencialDb.Visitas.Where(x => x.VisitanteDNI == model.VisitanteDNI);

            foreach (VisitaEntity visita in visitas)
            {
                visita.DarEntrada = model.DarEntrada;
            }

            _residencialDb.SaveChanges();

            return FindBy(x => x.VisitanteDNI == model.VisitanteDNI) != null;
        }
    }
}
