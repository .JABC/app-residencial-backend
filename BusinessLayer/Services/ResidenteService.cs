﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;
using WebApi.Core.Models;
using WebApi.Interfaces;
using WebApi.Utilities.Http;
using WebApi.Utilities.IQueryableExtensions;

namespace WebApi.Core.Services
{
    public class ResidenteService : IServiceMethod<ResidenteModel>
    {
        private readonly ResidencialDb _residencialDb;
        private readonly IMapper _mapper;

        public ResidenteService(ResidencialDb residencialDb, IMapper mapper)
        {
            _residencialDb = residencialDb;
            _mapper = mapper;
        }

        public ResidenteModel Add(ResidenteModel model)
        {

            var entity = _mapper.Map<ResidenteModel, ResidenteEntity>(model);

            if (_residencialDb.Residentes.Any(x => x.DNI == entity.DNI))
                throw new Exception("El DNI del Residente ya existe.");

            entity.CreateDate = DateTime.Now;

            _residencialDb.Residentes.Add(entity);
            _residencialDb.SaveChanges();

            return FindBy(x => x.Id == entity.Id);
        }

        public bool Delete(ResidenteModel model)
        {
            var item = _residencialDb.Residentes.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Existe");

            _residencialDb.Residentes.Remove(item);
            return _residencialDb.SaveChanges() > 0;
        }

        public IQueryable<ResidenteModel> Select()
        {
            var query = _residencialDb.Residentes.ProjectTo<ResidenteModel>(_mapper.ConfigurationProvider);
            return query;
        }

        public ResidenteModel FindBy(Func<ResidenteModel, bool> predicate)
        {
            return Select().Where(predicate).FirstOrDefault();
        }

        public List<ResidenteModel> GetPage(OperationRequest operation)
        {
            return Select()
                    .AddFilter(operation.Filters)
                    .AddSortBy(operation.Sorts)
                    .AddPagination(operation.Pagination);
        }

        public ResidenteModel Update(ResidenteModel model)
        {
            var item = _residencialDb.Residentes.FirstOrDefault(x => x.Id == model.Id);

            if (item == null)
                throw new Exception("Registro No Encontrado");

            _mapper.Map(model, item);

            item.UpdateDate = DateTime.Now;

            _residencialDb.SaveChanges();

            return FindBy(m => m.Id == item.Id);
        }

        public bool MoverA(ResidenteModel model)
        {
            var residentes = _residencialDb.Residentes.Where(x => x.ResidenciaId == model.ResidenciaId);
            var residencia = _residencialDb.Residencias.FirstOrDefault(x => x.Numero == model.ResidenciaNumero);

	    if (residencia == null) throw new Exception("Residencia no encontrada");

            foreach (ResidenteEntity residente in residentes)
            {
                residente.ResidenciaId = residencia.Id;
            }

            _residencialDb.SaveChanges();

            return FindBy(x => x.ResidenciaId == residencia.Id) != null;
        }

    }
}
