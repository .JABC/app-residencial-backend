﻿//using AutoMapper;
//using AutoMapper.QueryableExtensions;
//using BusinessLayer.Db;
//using BusinessLayer.Db.Entities;
//using BusinessLayer.Interfaces;
//using BusinessLayer.Models;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Text;

//namespace WebApi.Core.Services
//{
//    public class VisitaGrupoService: IServiceMethod<VisitaGrupoModel>
//    {
//        private readonly ResidencialDb _residencialDb;
//        private readonly IMapper _mapper;

//        public VisitaGrupoService(ResidencialDb residencialDb, IMapper mapper)
//        {
//            _residencialDb = residencialDb;
//            _mapper = mapper;
//        }

//        public int Add(VisitaGrupoModel model)
//        {

//            var entity = _mapper.Map<VisitaGrupoModel, VisitaGrupoEntity>(model);

//            entity.Fecha = DateTime.Now;

//            _residencialDb.VisitaGrupos.Add(entity);
//            _residencialDb.SaveChanges();

//            return entity.Id;
//        }

//        public bool Delete(VisitaGrupoModel model)
//        {
//            var item = _residencialDb.VisitaGrupos.FirstOrDefault(x => x.Id == model.Id);

//            if (item == null)
//                throw new Exception("Registro No Existe");

//            _residencialDb.VisitaGrupos.Remove(item);

//            return _residencialDb.SaveChanges() > 0;
//        }

//        public IEnumerable<VisitaGrupoModel> Select()
//        {
//            var query = _residencialDb.VisitaGrupos.ProjectTo<VisitaGrupoModel>(_mapper.ConfigurationProvider);
//            return query;
//        }

//        public VisitaGrupoModel FindBy(Func<VisitaGrupoModel, bool> predicate)
//        {
//            return Select().Where(predicate).FirstOrDefault();
//        }

//        public HttpGetResult<VisitaGrupoModel> GetPage(HttpGetRequest request)
//        {
//            var result = new HttpGetResult<VisitaGrupoModel>();
//            var paginate = new Paginate();

//            if (string.IsNullOrEmpty(request.FilterValue))
//            {

//                result.Data = Select();

//            }
//            else if (request.FilterValue.Contains(','))
//            { // Reparar solo acepta dos valores

//                var splitString = request.FilterValue.Split(',');
//                var items = new List<VisitaGrupoModel>();
//                foreach (var model in Select())
//                {
//                    try
//                    {
//                        var item = JsonConvert.SerializeObject(model).ToLower();
//                        if (item.Contains(splitString[0].Trim().ToLower())
//                            && item.Contains(splitString[1].Trim().ToLower()))
//                        {
//                            items.Add(model);
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        throw new Exception(e.Message);
//                    }
//                }

//                result.Data = items;

//            }
//            else
//            {

//                var items = new List<VisitaGrupoModel>();
//                foreach (var model in Select())
//                {
//                    var item = JsonConvert.SerializeObject(model).ToLower();
//                    if (item.Contains(request.FilterValue.ToLower()))
//                    {
//                        items.Add(model);
//                    }
//                }

//                result.Data = items;
//            }


//            paginate.CurrentPage = request.Page.CurrentPage;
//            paginate.Limit = request.Page.Limit;
//            paginate.Count = Select().Count();

//            result.Data = result.Data
//                            .Skip((paginate.CurrentPage - 1) * paginate.Limit)
//                            .Take(paginate.Limit).AsEnumerable();

//            result.Page = paginate;

//            return result;
//        }

//        public void Update(VisitaGrupoModel model)
//        {
//            var item = _residencialDb.VisitaGrupos.FirstOrDefault(x => x.Id == model.Id);

//            if (item == null)
//                throw new Exception("Registro No Encontrado");

//            _mapper.Map(model, item);

//            _residencialDb.SaveChanges();
//        }
//    }
//}
