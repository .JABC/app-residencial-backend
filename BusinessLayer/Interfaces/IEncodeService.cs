﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Interfaces
{
    public interface IEncodeService
    {
        string GenerateEncodeBytes();
        string EncodePassword(string encode, string password);
    }
}
