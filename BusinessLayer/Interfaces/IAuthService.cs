﻿using WebApi.Core.Models;

namespace WebApi.Interfaces
{
    public interface IAuthService
    {
        UserModel ValidateUser(UserModel model);
        //UserModel CreateUser(string username, string email, string password);
        //UserModel CreateUser(string username, string email, string password, string roleName);
        UserModel CreateUser(string username, string email, string password, int? roleId);
        bool IsUserValid(UserModel user, string password);
        bool IsPasswordValid(UserModel user, string password);
        bool ChangePassword(UserPasswordModel model);
        //IQueryable<RoleModel> getUserRole(int identity);
        //int AddRole(string roleName);
        //bool RemoveUserRole(int userId, int roleId);
    }
}
