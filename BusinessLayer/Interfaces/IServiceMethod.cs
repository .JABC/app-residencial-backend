﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Utilities.Http;

namespace WebApi.Interfaces
{
    public interface IServiceMethod<TModel>
    {
        TModel Add(TModel model);
        bool Delete(TModel model);
        TModel Update(TModel model);
        IQueryable<TModel> Select();
        TModel FindBy(Func<TModel, bool> predicate);
        List<TModel> GetPage(OperationRequest operation);

    }
}
