﻿using System.Collections.Generic;
using WebApi.Core.Models;

namespace WebApi.Interfaces
{
    public interface IStorageProvider
    {
        List<ResidenciaModel> Residencias { get; }
        List<ResidenteModel> Residentes { get; }
        void SaveData();
        void LoadData();

    }
}
