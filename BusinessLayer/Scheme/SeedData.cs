﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using WebApi.Core.Database;
using WebApi.Core.Database.Entities;

namespace WebApi.Core.Scheme
{
    public static class SeedData
    {
        public static TipoResidenteEntity[] GetTipoResidenteEntities()
        {
            return new TipoResidenteEntity[]
            {
                new TipoResidenteEntity { Id = 1, Descripcion = "Propietario" },
                new TipoResidenteEntity { Id = 2, Descripcion = "Inquilino" },
                new TipoResidenteEntity { Id = 3, Descripcion = "Habitante" }
            };
        }

        public static TipoResidenciaEntity[] GetTipoResidenciaEntities()
        {
            return new TipoResidenciaEntity[]
            {
                new TipoResidenciaEntity { Id = 1, Descripcion = "Apartamento" },
                new TipoResidenciaEntity { Id = 2, Descripcion = "Casa" },
                new TipoResidenciaEntity { Id = 3, Descripcion = "Negocio" }
            };
        }

        public static ParentescoEntity[] GetParentescoEntities()
        {
            return new ParentescoEntity[]
            {
                new ParentescoEntity { Id = 1, Descripcion = "Ninguno" },
                new ParentescoEntity { Id = 2, Descripcion = "Hermano" },
                new ParentescoEntity { Id = 3, Descripcion = "Esposo" },
                new ParentescoEntity { Id = 4, Descripcion = "Primo" },
                new ParentescoEntity { Id = 5, Descripcion = "Sobrino" },
                new ParentescoEntity { Id = 6, Descripcion = "Padre" },
                new ParentescoEntity { Id = 7, Descripcion = "Hijo" },
                new ParentescoEntity { Id = 8, Descripcion = "Otro" }
            };
        }

        public static RoleEntity[] GetRoleEntities()
        {
            return new RoleEntity[]
            {
                new RoleEntity { Id = 1, Name = "Administrador" },
                new RoleEntity { Id = 2, Name = "Guardia" },
                new RoleEntity { Id = 3, Name = "Residente" }
            };
        }

        public static void Seed(this ResidencialDb context)
        {
            context.Database.Migrate();
            if (!context.Calles.Any())
            {
                context.Calles.AddRange(
                    new CalleEntity { Nombre = "Moises" },
                    new CalleEntity { Nombre = "Isaac" },
                    new CalleEntity { Nombre = "David" },
                    new CalleEntity { Nombre = "Noe" }
                );
                context.SaveChanges();
            }

            //if (!context.TipoResidencias.Any())
            //{
            //    context.TipoResidencias.AddRange(
            //        new TipoResidenciaEntity { Descripcion = "Apartamento" },
            //        new TipoResidenciaEntity { Descripcion = "Casa" },
            //        new TipoResidenciaEntity { Descripcion = "Negocio" }
            //    );
            //    context.SaveChanges();
            //}

            //if (!context.TipoResidentes.Any())
            //{
            //    context.TipoResidentes.AddRange(
            //        new TipoResidenteEntity { Descripcion = "Propietario" },
            //        new TipoResidenteEntity { Descripcion = "Inquilino" },
            //        new TipoResidenteEntity { Descripcion = "Habitante" }
            //    );
            //    context.SaveChanges();
            //}

            //if (!context.Parentescos.Any())
            //{
            //    context.Parentescos.AddRange(
            //        new ParentescoEntity { Descripcion = "Ninguno" },
            //        new ParentescoEntity { Descripcion = "Hermano" },
            //        new ParentescoEntity { Descripcion = "Esposo" },
            //        new ParentescoEntity { Descripcion = "Primo" },
            //        new ParentescoEntity { Descripcion = "Sobrino" },
            //        new ParentescoEntity { Descripcion = "Padre" },
            //        new ParentescoEntity { Descripcion = "Hijo" },
            //        new ParentescoEntity { Descripcion = "Otro" }
            //    );
            //    context.SaveChanges();
            //}

            if (!context.Residentes.Any())
            {
                context.Residentes.AddRange(
                    new ResidenteEntity
                    {
                        Nombre = "Juan",
                        Apellido = "Perez",
                        Nacimiento = new DateTime(1994, 12, 20),
                        Genero = "M",
                        DNI = "00111111111",
                        TipoResidenteId = 1,
                        Activo = true,
                        CreateDate = DateTime.Now
                    },
                    new ResidenteEntity
                    {
                        Nombre = "Maria",
                        Apellido = "Perez",
                        Nacimiento = new DateTime(1989, 11, 10),
                        Genero = "F",
                        DNI = "00114511221",
                        TipoResidenteId = 1,
                        Activo = true,
                        CreateDate = DateTime.Now
                    },
                    new ResidenteEntity
                    {
                        Nombre = "Olga",
                        Apellido = "Sanchez",
                        Nacimiento = new DateTime(1974, 1, 20),
                        Genero = "F",
                        DNI = "00111241111",
                        TipoResidenteId = 2,
                        Activo = false,
                        CreateDate = DateTime.Now
                    },
                    new ResidenteEntity
                    {
                        Nombre = "Pablo",
                        Apellido = "Lopez",
                        Nacimiento = new DateTime(1992, 2, 1),
                        Genero = "M",
                        DNI = "00113421111",
                        TipoResidenteId = 3,
                        Activo = true,
                        CreateDate = DateTime.Now
                    },
                    new ResidenteEntity
                    {
                        Nombre = "Juana",
                        Apellido = "Lopez",
                        Nacimiento = new DateTime(1993, 5, 20),
                        Genero = "F",
                        DNI = "00132111141",
                        TipoResidenteId = 2,
                        Activo = true,
                        CreateDate = DateTime.Now
                    }
                );
                context.SaveChanges();
            }

            if (!context.Residencias.Any())
            {
                context.Residencias.AddRange(
                    new ResidenciaEntity
                    {
                        UniqueIdentifier = Guid.NewGuid().ToString("x").GetHashCode().ToString("x"),
                        Numero = "240",
                        CalleId = 1,
                        PropietarioId = 1,
                        TipoResidenciaId = 2,
                        Piso = null,
                        Poblada = false,
                        CreateDate = DateTime.Now
                    },
                    new ResidenciaEntity
                    {
                        UniqueIdentifier = Guid.NewGuid().ToString("x").GetHashCode().ToString("x"),
                        Numero = "261",
                        CalleId = 1,
                        PropietarioId = 1,
                        TipoResidenciaId = 2,
                        Poblada = false,
                        Piso = null,
                        CreateDate = DateTime.Now
                    },
                    new ResidenciaEntity
                    {
                        UniqueIdentifier = Guid.NewGuid().ToString("x").GetHashCode().ToString("x"),
                        Numero = "234",
                        CalleId = 3,
                        PropietarioId = 2,
                        TipoResidenciaId = 1,
                        Poblada = false,
                        Piso = 1,
                        CreateDate = DateTime.Now
                    },
                    new ResidenciaEntity
                    {
                        UniqueIdentifier = Guid.NewGuid().ToString("x").GetHashCode().ToString("x"),
                        Numero = "234A",
                        CalleId = 3,
                        PropietarioId = 1,
                        TipoResidenciaId = 1,
                        Poblada = false,
                        Piso = 2,
                        CreateDate = DateTime.Now
                    }
                );
                context.SaveChanges();
            }

            if (!context.Vehiculos.Any())
            {
                context.Vehiculos.AddRange(
                    new VehiculoEntity
                    {
                        Matricula = "123D",
                        Descripcion = "Toyota Gris",
                        ResidenteId = 1
                    },
                    new VehiculoEntity
                    {
                        Matricula = "A123",
                        Descripcion = "Mazda Azul",
                        ResidenteId = 2
                    }
                );
                context.SaveChanges();
            }

            //if (!context.Visitantes.Any())
            //{
            //    context.Visitantes.AddRange(
            //        new VisitanteEntity
            //        {
            //            Nombre = "Genaro Lopez",
            //            DNI = "123123123",
            //            Matricula = "O012d",
            //            Descripcion = "Vehiculo: Nissan Azul",
            //            Reportado = false,
            //        },
            //        new VisitanteEntity
            //        {
            //            Nombre = "Ana Garcia",
            //            DNI = "0010000123",
            //            Matricula = "ABC1",
            //            Descripcion = "Vehiculo: Honda Verde",
            //            Reportado = false
            //        }
            //    );
            //    context.SaveChanges();
            //}

            if (!context.Visitas.Any())
            {
                context.Visitas.AddRange(
                    new VisitaEntity
                    {
                        ResidenciaId = 1,
                        Llegada = DateTime.Now.AddDays(4),
                        Salida = DateTime.Now.AddDays(4).AddMinutes(30),
                        VisitanteDNI = "00123123123",
                        VisitanteNombre = "Genaro Lopez",
                        Motivo = "Personal"
                    },
                    new VisitaEntity
                    {
                        ResidenciaId = 1,
                        Llegada = DateTime.Now.AddDays(1),
                        Salida = DateTime.Now.AddDays(1).AddMinutes(30),
                        VisitanteDNI = "00123123123",
                        VisitanteNombre = "Genaro Lopez",
                        Motivo = "Personal"
                    },
                    new VisitaEntity
                    {
                        ResidenciaId = 1,
                        Llegada = DateTime.Now.AddDays(7),
                        Salida = DateTime.Now.AddDays(7).AddMinutes(30),
                        VisitanteDNI = "11111111111",
                        VisitanteNombre = "Dilcia Panda",
                        Motivo = "Familiar",
                        Reportado = true
                    }
                );
                context.SaveChanges();
            }

            //if (!context.Roles.Any())
            //{
            //    context.Roles.AddRange(
            //        new RoleEntity
            //        {
            //            Id = 1,
            //            Name = "Administrador"
            //        },
            //        new RoleEntity
            //        {
            //            Id = 2,
            //            Name = "Guardia"
            //        },
            //        new RoleEntity
            //        {
            //            Id = 3,
            //            Name = "Residente"
            //        }
            //    );
            //    context.SaveChanges();

            //}

            if (!context.Users.Any())
            {
                context.Users.Add(
                    new UserEntity
                    {
                        RoleId = 1,
                        UserName = "Admin",
                        Email = "admin@admin.com",
                        Password = "JXnsq3vT/7/C46Kyz/IE9qIGleciuqEvsq9VhlxRa9Y=",
                        EncodeString = "ZwNhQubWdP/IjgPcXlx1WQ==",
                        IsLocked = false,
                        CreateDate = DateTime.Now
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
