﻿using Microsoft.EntityFrameworkCore;
using WebApi.Core.Database.Entities;

namespace WebApi.Core.Scheme
{
    public static class EntityBuilder
    {
        public static void EntityConfig(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CalleEntity>(entity => {

                entity.ToTable("Calle");
                entity.HasKey(c => c.Id);
                entity.Property(c => c.Id).ValueGeneratedOnAdd();
                entity.Property(p => p.Nombre).HasMaxLength(20);
            });

            modelBuilder.Entity<ResidenciaEntity>(entity => {

                entity.ToTable("Residencia");
                entity.HasKey(r => r.Id);
                entity.Property(r => r.Id).ValueGeneratedOnAdd();
                //entity.HasOne(r => r.Calle)
                //        .WithMany()
                //        .HasForeignKey(r => r.CalleId)
                //        .OnDelete(DeleteBehavior.Restrict);
                //entity.HasOne(r => r.TipoResidencia)
                //        .WithMany()
                //        .HasForeignKey(r => r.TipoResidenciaId)
                //        .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(r => r.Propietario)
                        .WithMany(r => r.GetResidencias)
                        .HasForeignKey(r => r.PropietarioId)
                        .OnDelete(DeleteBehavior.SetNull);
                entity.HasMany(r => r.GetResidentes)
                        .WithOne(r => r.Residencia)
                        .HasForeignKey(r => r.ResidenciaId)
                        .OnDelete(DeleteBehavior.SetNull);
                entity.HasIndex(x => new { x.Numero, x.CalleId })
                    .IsUnique();
            });

            modelBuilder.Entity<TipoResidenciaEntity>(entity => {

                entity.ToTable("TipoResidencia");
                entity.HasKey(tr => tr.Id);
                entity.Property(tr => tr.Id).ValueGeneratedNever();
                entity.Property(p => p.Descripcion).HasMaxLength(20);
                entity.HasData(SeedData.GetTipoResidenciaEntities());
            });

            modelBuilder.Entity<ResidenteEntity>(entity => {

                entity.ToTable("Residente");
                entity.HasKey(r => r.Id);
                entity.Property(c => c.Id).ValueGeneratedOnAdd();
                entity.Property(p => p.Nombre).HasMaxLength(20);
                entity.Property(p => p.Apellido).HasMaxLength(20);
                entity.Property(p => p.Nacimiento).HasColumnType("date");
                entity.Property(p => p.Genero).HasMaxLength(1);
                entity.HasIndex(p => p.DNI).IsUnique();
                //entity.HasOne(r => r.TipoResidente)
                //        .WithMany()
                //        .HasForeignKey(r => r.TipoResidenteId)
                //        .OnDelete(DeleteBehavior.Restrict);
                //entity.HasOne(r => r.Parentesco)
                //        .WithMany()
                //        .HasForeignKey(r => r.ParentescoId)
                //        .OnDelete(DeleteBehavior.Restrict);
                //entity.HasOne(r => r.Residencia)
                //        .WithMany()
                //        .HasForeignKey(r => r.ResidenciaId)
                //        .OnDelete(DeleteBehavior.Restrict);
                //entity.HasOne(r => r.Residencia)
                //        .WithMany()
                //        .HasForeignKey(r => r.ResidenciaId)
                //        .OnDelete(DeleteBehavior.Restrict);
                entity.HasIndex(r => r.DNI).IsUnique();
            });

            modelBuilder.Entity<TipoResidenteEntity>(entity => {

                entity.ToTable("TipoResidente");
                entity.HasKey(tr => tr.Id);
                entity.Property(tr => tr.Id).ValueGeneratedNever();
                entity.Property(p => p.Descripcion).HasMaxLength(20);
                entity.HasData(SeedData.GetTipoResidenteEntities());

            });

            modelBuilder.Entity<VehiculoEntity>(entity => {

                entity.ToTable("Vehiculo");
                entity.HasKey(v => v.Id);
                entity.Property(v => v.Id).ValueGeneratedOnAdd();
                entity.Property(p => p.Matricula).HasMaxLength(20);
                entity.Property(p => p.Descripcion).HasMaxLength(30);
                //entity.HasOne(v => v.Residencia)
                //        .WithMany()
                //        .HasForeignKey(v => v.ResidenciaId)
                //        .OnDelete(DeleteBehavior.Restrict);

            });

            modelBuilder.Entity<ParentescoEntity>(entity => {

                entity.ToTable("Parentesco");
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Id).ValueGeneratedNever();
                entity.Property(p => p.Descripcion).HasMaxLength(30);
                entity.HasData(SeedData.GetParentescoEntities());

            });

            //modelBuilder.Entity<VisitanteEntity>(entity => {

            //    entity.ToTable("Visitante");
            //    entity.HasKey(v => v.Id);
            //    entity.Property(v => v.Id).ValueGeneratedOnAdd();
            //    entity.Property(p => p.Nombre).HasMaxLength(20);
            //    entity.Property(p => p.Matricula).HasMaxLength(20);
            //    entity.HasIndex(r => r.DNI).IsUnique();

            //});

            modelBuilder.Entity<VisitaEntity>(entity => {

                entity.ToTable("Visita");
                entity.HasKey(v => v.Id);
                entity.Property(v => v.Id).ValueGeneratedOnAdd();
                entity.Property(p => p.Motivo).HasMaxLength(15);

            });

            //modelBuilder.Entity<VisitaGrupoEntity>(entity =>
            //{
            //    entity.ToTable("VisitaGrupo");
            //    entity.HasKey(vg => vg.Id);
            //    entity.Property(vg => vg.Id).ValueGeneratedOnAdd();
            //});

            modelBuilder.Entity<UserEntity>(entity =>
            {
                entity.ToTable("User");
                entity.HasKey(u => u.Id);
                entity.Property(u => u.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<RoleEntity>(entity =>
            {
                entity.ToTable("Roles");
                entity.HasKey(u => u.Id);
                entity.Property(u => u.Id).ValueGeneratedNever();
                entity.HasData(SeedData.GetRoleEntities());
            });

            //modelBuilder.Entity<UserInRoleEntity>(entity =>
            //{
            //    entity.ToTable("UserInRole");
            //    entity.HasKey(u => u.Id);
            //    entity.Property(u => u.Id).ValueGeneratedOnAdd();
            //});

        }
    }
}
