﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Core.Models
{
    public class UserInRoleModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public RoleModel Role { get; set; }
    }
}
