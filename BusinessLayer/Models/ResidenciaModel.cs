﻿using System;
using System.Collections.Generic;

namespace WebApi.Core.Models
{
    public class ResidenciaModel
    {

        public int Id { get; set; }
        public string Numero { get; set; }
        public int? PropietarioId { get; set; }
        public string PropietarioNombre { get; set; }
        public string PropietarioApellido { get; set; }
        public string PropietarioDNI { get; set; }
        public int? CalleId { get; set; }
        public string CalleNombre { get; set; }
        public int? TipoResidenciaId { get; set; }
        public string TipoResidenciaDescripcion { get; set; }
        public int? Piso { get; set; }
        public bool Poblada { get; set; }

        public ICollection<ResidenteModel> Residentes { get; set; }
        public ICollection<EtiquetaModel> Etiquetas { get; set; }
        public ICollection<VisitaModel> Visitas { get; set; }

        public string PropietarioNombreCompleto => $"{PropietarioNombre} {PropietarioApellido}";

        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public string UniqueIdentifier { get; set; }
    }

    public enum ETipoResidencia
    {
        Apartamento = 1,
        Casa,
        Negocio
    }
}
