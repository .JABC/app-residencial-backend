﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Core.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public enum ERoles
    {
        Administrador = 1,
        Guardia,
        Residente
    }
}
