﻿using Newtonsoft.Json;
using System;

namespace WebApi.Core.Models
{
    public class VisitaModel
    {
        public int Id { get; set; }
        public string VisitanteDNI { get; set; }
        public string VisitanteNombre { get; set; }
        public string Motivo { get; set; }
        public string Llegada { get; set; }
        public string Salida { get; set; }
        public string Nota { get; set; }
        public bool Reportado { get; set; }
        public DateTime? DarEntrada { get; set; }

        public bool TieneEntrada {
            get { return DarEntrada > DateTime.Now; }
            private set { }
        }

        public int? ResidenciaId { get; set; }
        public string ResidenciaNumero { get; set; }

    }
}
