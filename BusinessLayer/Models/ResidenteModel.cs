﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WebApi.Core.Database.Entities;

namespace WebApi.Core.Models
{
    public class ResidenteModel
    {
        public int Id { get; set; }
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int? ResidenciaId { get; set; }
        public int? TipoResidenteId { get; set; }
        public string TipoResidenteDescripcion { get; set; }
        public int? ParentescoId { get; set; }
        public string ParentescoDescripcion { get; set; }
        public string Telefono { get; set; }
        public string Nacimiento { get; set; }

        public string Genero { get; set; }
        public bool Activo { get; set; }

        public string NombreCompleto => $"{Nombre} {Apellido}";

        public string ResidenciaNumero { get; set; }
        [JsonIgnore] public string ResidenciaPropietarioNombre { get; set; }
        [JsonIgnore] public string ResidenciaPropietarioApellido { get; set; }

        public string ResidenciaPropietarioNombreCompleto
        {
            get
            {
                return $"{ResidenciaPropietarioNombre} {ResidenciaPropietarioApellido}";
            }
        }

        public ICollection<ResidenciaEntity> Residencias { get; set; }
        public ICollection<VehiculoModel> Vehiculos { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public enum ETipoResidente
    {
        Propietario = 1,
        Inquilino,
        Habitante
    }
}
