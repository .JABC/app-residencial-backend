﻿using System.Collections.Generic;
using WebApi.Core.Database.Entities;

namespace WebApi.Core.Models
{
    public class CalleModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Distancia { get; set; }

        public ICollection<ResidenciaEntity> Residencias { get; set; }

    }
}
