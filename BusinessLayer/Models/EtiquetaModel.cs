﻿namespace WebApi.Core.Models
{
    public class EtiquetaModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public int Activo { get; set; }
    }
}
