﻿using AutoMapper;
using System;
using WebApi.Core.Database.Entities;

namespace WebApi.Core.Models
{
    public class EntityProfile: Profile
    {
        public EntityProfile()
        {
            CreateMap<DateTime?, string>().ConvertUsing(d => d == null ? null : d.Value.ToString("yyyy-MM-ddThh:mm"));
            CreateMap<ResidenciaModel, ResidenciaEntity>().ReverseMap();
            CreateMap<ResidenteModel, ResidenteEntity>().ReverseMap();
            CreateMap<CalleModel, CalleEntity>().ReverseMap();
            CreateMap<VehiculoModel, VehiculoEntity>().ReverseMap();
            CreateMap<VisitaModel, VisitaEntity>().ReverseMap();
            CreateMap<UserModel, UserEntity>().ReverseMap();
            CreateMap<RoleModel, RoleEntity>().ReverseMap();
        }
    }
}
