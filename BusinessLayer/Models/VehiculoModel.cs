﻿namespace WebApi.Core.Models
{
    public class VehiculoModel
    {
        public int Id { get; set; }
        public int? ResidenteId { get; set; }
        public string Matricula { get; set; }
        public string Descripcion { get; set; }

	    public string ResidenteNombre { get; set; }
	    public string ResidenteApellido { get; set; }

	    public string ResidenteNombreCompleto { 
            get {
                return $"{ResidenteNombre} {ResidenteApellido}";
            }
        }
    }
}
