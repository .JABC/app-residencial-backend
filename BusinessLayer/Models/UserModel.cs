﻿using Newtonsoft.Json;
using System;

namespace WebApi.Core.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public int? RoleId { get; set; }
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string EncodeString { get; set; }
        //[JsonIgnore] public string[] Claims { get; set; }
        public bool IsLocked { get; set; }
        public DateTime? CreateDate { get; set; }

        //[JsonIgnore] public ICollection<UserInRoleModel> UserInRoles { get; set; }
    }

    public class UserPasswordModel
    {
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
