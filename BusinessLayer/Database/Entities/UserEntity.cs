﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string EncodeString { get; set; }
        public bool IsLocked { get; set; }
        public DateTime? CreateDate { get; set; }

        #region Foreign Key
        public int? RoleId { get; set; }
        public RoleEntity Role { get; set; }
        #endregion

    }
}
