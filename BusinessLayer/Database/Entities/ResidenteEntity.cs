﻿using System;
using System.Collections.Generic;
using WebApi.Utilities.Interfaces;

namespace WebApi.Core.Database.Entities
{
    public class ResidenteEntity: IEntity<int>
    {

        public int Id { get; set; }
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public DateTime? Nacimiento { get; set; }
        public string Genero { get; set; }
        public bool Activo { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        #region Foreign Keys
        public int? TipoResidenteId { get; set; }
        public TipoResidenteEntity TipoResidente { get; set; }

        public int? ParentescoId { get; set; }
        public ParentescoEntity Parentesco { get; set; }

        public int? ResidenciaId { get; set; }
        public ResidenciaEntity Residencia { get; set; } // Residentes

        public ICollection<ResidenciaEntity> GetResidencias { get; set; } // Propietario

        public ICollection<VehiculoEntity> GetVehiculos { get; set; }
        #endregion
    }
}
