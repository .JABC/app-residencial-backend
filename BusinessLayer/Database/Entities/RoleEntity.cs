﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class RoleEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        #region Foreign Key
        public ICollection<UserEntity> Users { get; set; }
        #endregion
    }
}
