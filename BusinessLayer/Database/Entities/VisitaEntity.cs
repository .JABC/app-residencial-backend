﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class VisitaEntity
    {
        public int Id { get; set; }
        public string VisitanteDNI { get; set; }
        public string VisitanteNombre { get; set; }
        public string Motivo { get; set; }
        public DateTime? Llegada { get; set; }
        public DateTime? Salida { get; set; }
        public string Nota { get; set; }
        public bool Reportado { get; set; }
        public DateTime? DarEntrada { get; set; }

        #region Foreign Key
        public int? ResidenciaId { get; set; }
        public ResidenciaEntity Residencia { get; set; }
        #endregion
    }
}
