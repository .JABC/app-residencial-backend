﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class TipoResidenteEntity
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

        public ICollection<ResidenteEntity> Residentes { get; set; }
    }
}
