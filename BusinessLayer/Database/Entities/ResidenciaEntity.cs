﻿using System;
using System.Collections.Generic;
using WebApi.Utilities.Interfaces;

namespace WebApi.Core.Database.Entities
{
    public class ResidenciaEntity: IEntity<int>
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        //public int? EtiquetaId { get; set; }
        public int? Piso { get; set; }
        public bool Poblada { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public string UniqueIdentifier { get; set; }

        #region Foreign Key
        public int? PropietarioId { get; set; }
        public ResidenteEntity Propietario { get; set; }

        public int? CalleId { get; set; }
        public CalleEntity Calle { get; set; }

        public int? TipoResidenciaId { get; set; }
        public TipoResidenciaEntity TipoResidencia { get; set; }

        public ICollection<ResidenteEntity> GetResidentes { get; set; }
        public ICollection<VehiculoEntity> GetVehiculos { get; set; }
        //public ICollection<EtiquetaEntity> GetEtiquetas { get; set; }
        public ICollection<VisitaEntity> GetVisitas { get; set; }
        #endregion
    }

}
