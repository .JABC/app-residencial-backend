﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class VehiculoEntity
    {
        public int Id { get; set; }
        public string Matricula { get; set; }
        public string Descripcion { get; set; }

        #region Foreign Key
        public int? ResidenteId { get; set; }
        public ResidenteEntity Residente { get; set; }
        #endregion

    }
}
