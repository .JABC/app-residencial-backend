﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class ParentescoEntity
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

        #region Foreign Key
        public ICollection<ResidenteEntity> Residentes { get; set; }
        #endregion  
    }
}
