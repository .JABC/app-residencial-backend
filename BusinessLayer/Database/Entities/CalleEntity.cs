﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebApi.Core.Database.Entities
{
    public class CalleEntity
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Distancia { get; set; }

        #region Foreign Key
        public ICollection<ResidenciaEntity> GetResidencias { get; set; }
        #endregion
    }
}
