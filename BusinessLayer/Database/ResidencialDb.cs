﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApi.Core.Database.Entities;
using WebApi.Core.Scheme;

namespace WebApi.Core.Database
{
    public class ResidencialDb: DbContext
    {
        private readonly IConfiguration _configuration;

        public ResidencialDb(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration["Data:ConnectionString"]);
        }


        public DbSet<ResidenciaEntity> Residencias { get; set; }
        public DbSet<ResidenteEntity> Residentes { get; set; }
        public DbSet<CalleEntity> Calles { get; set; }
        public DbSet<VehiculoEntity> Vehiculos { get; set; }
        public DbSet<TipoResidenciaEntity> TipoResidencias { get; set; }
        public DbSet<TipoResidenteEntity> TipoResidentes { get; set; }
        public DbSet<ParentescoEntity> Parentescos { get; set; }
        //public DbSet<VisitanteEntity> Visitantes { get; set; }
        public DbSet<VisitaEntity> Visitas { get; set; }
        //public DbSet<VisitaGrupoEntity> VisitaGrupos { get; set; }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
        //public DbSet<UserInRoleEntity> UserInRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.EntityConfig();
        }

    }
}